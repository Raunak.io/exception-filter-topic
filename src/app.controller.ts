import { HttpExceptionFilter } from './http-exception.filter';
import { ForbiddenError } from './practice.exception';
import { Controller, Get ,HttpStatus, HttpException, UseFilters,InternalServerErrorException} from '@nestjs/common';
import { AppService } from './app.service';


@Controller()
@UseFilters(HttpExceptionFilter) // can add multiple only seperated by , can also use at every method but not preffered coz of more memory usage

export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  async getHello(): Promise<any>{
try{

    return this.appService.getHello();
}catch(err){
  // throw new HttpException('Forbidden',HttpStatus.FORBIDDEN);//OR WE CAN PASS AN OBJECT
  throw new HttpException({
    status:HttpStatus.FORBIDDEN,
    errorMessage:'there is something wrong ',
    error:err
  },HttpStatus.FORBIDDEN);
// throw new ForbiddenError() // custom exception filter 
// throw new InternalServerErrorException('thre is something wrong with this server') // an example of built in exception 
 // check main.ts file also
}

  }
}
