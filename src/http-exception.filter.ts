import {
  ExceptionFilter,
  HttpException,
  ArgumentsHost,
  Catch,
 
} from '@nestjs/common';
import { Response, Request } from 'express';

@Catch(HttpException)  // binds the required metadata to the filter ,can take single or multi parameters
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost): any {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();
    const status = exception.getStatus();

    response.status(status).json({
      statusCode: status,
      methodUsed:request.method, // which method is used 
    
      path: request.url, // the path

      description: exception.message,
      stack: exception.stack, // stack gives full stack trace for better debugging
    });
  }
}
